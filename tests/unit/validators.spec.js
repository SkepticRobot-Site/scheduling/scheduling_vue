import { searchValidator, pickValidator } from '../../src/bin/validators'

import xml2js from 'xml2js'

import readFile from 'fs/promises'

import chai from 'chai'

var gloomhaven = '/home/webapps/scheduling/scheduling_vue/tests/resources/xml/gloomhaven'

function parse(data,passedValidator){
    console.log('parse')
    var parser = new xml2js.Parser({validator:passedValidator})
    var parsed
    parser
    .parseStringPromise(data)
    .then(p =>{
        return p
    })
    .catch(error=>{
        console.log(error)
    })
}

function getFile(base,part ='',ext='.xml'){
    console.log('get file')
    var s = base+part+ext
    console.log(s)
    var p = readFile(s)
    p.then(data =>{
        console.log(data)
        return data
    })
    .catch(error=>{
        console.log(error)
    })

}

function checkPickValidator(base,part='',ext='.xml'){
    //get data and parse
    var data = getFile(base,part,ext)
    console.log(data)
    //var parsed = parse(data,pickValidator)
    //console.log(parsed)
    /*
    
    //check properties exist
    chai.expect(parsed).to.have.property('items')
    chai.expect(parsed.items).to.have.property('item')
    //item properties
    chai.expect(parsed.items.item[0]).to.have.property('$')
    chai.expect(parsed.items.item[0]).to.have.property('minplayers')
    chai.expect(parsed.items.item[0]).to.have.property('maxplayers')
    chai.expect(parsed.items.item[0]).to.have.property('playtime')
    chai.expect(parsed.items.item[0]).to.have.property('statistics')
    chai.expect(parsed.items.item[0]).to.have.property('poll')
    //minplayer properties
    chai.expect(parsed.items.item[0].minplayers[0]).to.have.property('$')
    chai.expect(parsed.items.item[0].minplayers[0].$).to.have.property('value')
    //maxplayer properties
    chai.expect(parsed.items.item[0].maxplayers[0]).to.have.property('$')
    chai.expect(parsed.items.item[0].maxplayers[0].$).to.have.property('value')
    //playtime properties
    chai.expect(parsed.items.item[0].playtime[0]).to.have.property('$')
    chai.expect(parsed.items.item[0].playtime[0].$).to.have.property('value')
    //statistics properites
    chai.expect(parsed.items.item[0].statistics[0]).to.have.property('ratings')
    //ratings properties
    chai.expect(parsed.items.item[0].statistics[0].ratings[0]).to.have.property('average')
    chai.expect(parsed.items.item[0].statistics[0].ratings[0]).to.have.property('averageweight')
    //average properties
    chai.expect(parsed.items.item[0].statistics[0].ratings[0].average[0]).to.have.property('$')
    chai.expect(parsed.items.item[0].statistics[0].ratings[0].average[0].$).to.have.property('value')
    //average weight properties
    chai.expect(parsed.items.item[0].statistics[0].ratings[0].averageweight[0]).to.have.property('$')
    chai.expect(parsed.items.item[0].statistics[0].ratings[0].averageweight[0].$).to.have.property('value')
    //poll properties
    chai.expect(parsed.items.item[0].poll[0]).to.have.property('$')
    chai.expect(parsed.items.item[0].poll[0].$).to.have.property('name')

    //check arrays have length
    chai.expect(parsed.items.item).to.have.lengthOf.at.least(1)
    chai.expect(parsed.items.item[0].minplayers).to.have.lengthOf.at.least(1)
    chai.expect(parsed.items.item[0].maxplayers).to.have.lengthOf.at.least(1)
    chai.expect(parsed.items.item[0].playtime).to.have.lengthOf.at.least(1)
    chai.expect(parsed.items.item[0].statistics).to.have.lengthOf.at.least(1)
    chai.expect(parsed.items.item[0].poll).to.have.lengthOf.at.least(1)
    chai.expect(parsed.items.item[0].statistics[0].ratings).to.have.lengthOf.at.least(1)
    chai.expect(parsed.items.item[0].statistics[0].ratings[0].average).to.have.lengthOf.at.least(1)
    chai.expect(parsed.items.item[0].statistics[0].ratings[0].averageweight).to.have.lengthOf.at.least(1)

    //check value types
    chai.expect(parsed.items.item[0].minplayers[0].$.value).to.be.a('number')
    chai.expect(parsed.items.item[0].maxplayers[0].$.value).to.be.a('number')
    chai.expect(parsed.items.item[0].playtime[0].$.value).to.be.a('number')
    chai.expect(parsed.items.item[0].statistics[0].ratings[0].average[0].$.value).to.be.a('number')
    chai.expect(parsed.items.item[0].statistics[0].ratings[0].averageweight[0].$.value).to.be.a('number')
    */
}


checkPickValidator(gloomhaven)
checkPickValidator(gloomhaven,'broken')

/*validation test
            var testXML = `<items total="187" termsofuse="https://boardgamegeek.com/xmlapi/termsofuse">
                    <item type="boardgame" id="318136">
                    <name type="primary" value="Apothecaries (fan expansion for Carcassonne)"/>
                    <yearpublished value="2014"/>
                    </item>
                    <item type="boardgame" id="248789">
                    <name type="primary" value="Apothecaries and Tithes (fan expansion for Carcassonne)"/>
                    </item>
                    <item type="boardgame" id="436243">
                    <yearpublished value="2014"/>
                    </item>
                    </items>`
            parser.parseStringPromise(testXML)
            .then(parsed=>{
                console.log(parsed)
            })*/